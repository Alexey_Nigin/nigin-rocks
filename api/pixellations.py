from flask import Blueprint, request, jsonify
from base64 import b64encode
from hashlib import sha256
import json
from jsonschema import validate, ValidationError
import random
import string
from . import state


blueprint = Blueprint('pixellations', __name__, url_prefix='/pixellations')

BOARD_KEY_BASE = 'pixellations.board'
CHAT_KEY = 'pixellations.chat'

NUM_COLORS = 8
MAX_LOCKS = 255

HASH_LENGTH = 16

# Number of chat messages to save in memory
NUM_MESSAGES_TO_SAVE = 1024
# Number of chat messages to send in a response
NUM_MESSAGES_TO_SEND = 256


class Tile:
	BASE_64_ALPHABET = (
		string.ascii_uppercase + string.ascii_lowercase + string.digits + '-_'
	)
	# Size of a tile in one dimension
	SIZE = 32

	def __init__(self, x, y, state_manager=state, known_hash=None,
					randomize=False):
		self.x = x
		self.y = y
		self._state_manager = state_manager
		self._known_hash = known_hash

		if randomize:
			self._pixels = [
				[
					{'color': random.randrange(NUM_COLORS), 'num_locks': 0}
					for y in range(Tile.SIZE)
				]
				for x in range(Tile.SIZE)
			]
			self._reconstruct_zip()
		else:
			self._zip = self._state_manager.get(
				f'{BOARD_KEY_BASE}.{self.x}.{self.y}'
			)
			self._create_pixels()

		self._unsaved_changes = randomize

	def get_api_hash_zip(self):
		'''
		Get hash+zip of the tile to return in the API, or None if the tile
		doesn't need to be re-sent
		'''
		new_hash = self._get_hash()
		return None if (new_hash == self._known_hash) else new_hash + self._zip

	def get_pixel_color(self, x, y):
		return self._pixels[x][y]['color']

	def get_pixel_num_locks(self, x, y):
		return self._pixels[x][y]['num_locks']

	def set_pixel_color(self, x, y, color):
		assert 0 <= color < NUM_COLORS, 'invalid color'
		self._pixels[x][y]['color'] = color
		self._reconstruct_zip()
		self._unsaved_changes = True

	def set_pixel_num_locks(self, x, y, num_locks):
		assert 0 <= num_locks <= MAX_LOCKS, 'invalid num_locks'
		self._pixels[x][y]['num_locks'] = num_locks
		self._reconstruct_zip()
		self._unsaved_changes = True

	def save(self):
		if self._unsaved_changes:
			self._state_manager.set(
				f'{BOARD_KEY_BASE}.{self.x}.{self.y}', self._zip
			)
			self._unsaved_changes = False

	def _create_pixels(self):
		'''
		Create self._pixels from self._zip
		'''
		self._pixels = [
			[None for y in range(Tile.SIZE)]
			for x in range(Tile.SIZE)
		]

		pointer = 0

		def read_char():
			nonlocal pointer
			assert pointer < len(self._zip), \
							f'zip ({self._zip}) is incomplete'
			char = self._zip[pointer]
			pointer += 1
			assert char in Tile.BASE_64_ALPHABET, f'char ({char}) is invalid'
			return Tile.BASE_64_ALPHABET.index(char)

		for y in range(Tile.SIZE):
			for x in range(Tile.SIZE):
				char_0 = read_char()
				if char_0 < 32:
					single_number = char_0
				else:
					char_1 = read_char()
					single_number = 64 * (char_0 - 32) + char_1
				color = single_number % NUM_COLORS
				num_locks = single_number // NUM_COLORS
				self._pixels[x][y] = {'color': color, 'num_locks': num_locks}

		assert pointer == len(self._zip), '_zip has extra data'

	def _reconstruct_zip(self):
		'''
		Reconstruct self._zip from self._pixels

		Call internally after every modification to self._pixels.
		'''
		zip_list = []

		for y in range(Tile.SIZE):
			for x in range(Tile.SIZE):
				color = self._pixels[x][y]['color']
				num_locks = self._pixels[x][y]['num_locks']
				single_number = NUM_COLORS * num_locks + color

				# Update this if NUM_COLORS * (MAX_LOCKS + 1) ever becomes >4096
				if single_number < 32:
					zip_list.append(Tile.BASE_64_ALPHABET[single_number])
				else:
					zip_list.append(
						Tile.BASE_64_ALPHABET[32 + single_number // 64]
						+ Tile.BASE_64_ALPHABET[single_number % 64]
					)

		self._zip = ''.join(zip_list)

	def _get_hash(self):
		return b64encode(
			sha256(self._zip.encode()).digest()
		).decode()[:HASH_LENGTH]


class Board:
	# Number of tiles in one dimension
	NUM_TILES = 8

	def __init__(self, state_manager=state, known_hashes=None,
					randomize_all=False):
		if known_hashes is None:
			known_hashes_list = [
				[None for y in range(self.NUM_TILES)]
				for x in range(self.NUM_TILES)
			]
		else:
			assert(len(known_hashes) == HASH_LENGTH * self.NUM_TILES ** 2)
			known_hashes_list = [
				[
					known_hashes[
						HASH_LENGTH * (x + y * self.NUM_TILES) :
						HASH_LENGTH * (x + y * self.NUM_TILES + 1)
					]
					for y in range(self.NUM_TILES)
				]
				for x in range(self.NUM_TILES)
			]

		self._tiles = [
			[
				Tile(
					x, y, state_manager=state_manager,
					known_hash=known_hashes_list[x][y], randomize=randomize_all
				)
				for y in range(self.NUM_TILES)
			]
			for x in range(self.NUM_TILES)
		]

	def get_api_data(self):
		'''
		Get data to return in the API
		'''
		return [
			self._tiles[i % self.NUM_TILES][i // self.NUM_TILES]\
				.get_api_hash_zip()
			for i in range(self.NUM_TILES ** 2)
		]

	def get_pixel_color(self, x, y):
		assert 0 <= x < Tile.SIZE * Board.NUM_TILES, f'wrong x: {x}'
		assert 0 <= y < Tile.SIZE * Board.NUM_TILES, f'wrong y: {y}'
		return self._tiles[x // Tile.SIZE][y // Tile.SIZE].get_pixel_color(
			x % Tile.SIZE, y % Tile.SIZE
		)

	def get_pixel_num_locks(self, x, y):
		assert 0 <= x < Tile.SIZE * Board.NUM_TILES, f'wrong x: {x}'
		assert 0 <= y < Tile.SIZE * Board.NUM_TILES, f'wrong y: {y}'
		return self._tiles[x // Tile.SIZE][y // Tile.SIZE].get_pixel_num_locks(
			x % Tile.SIZE, y % Tile.SIZE
		)

	def set_pixel_color(self, x, y, color):
		assert 0 <= x < Tile.SIZE * Board.NUM_TILES, f'wrong x: {x}'
		assert 0 <= y < Tile.SIZE * Board.NUM_TILES, f'wrong y: {y}'
		assert 0 <= color < NUM_COLORS, 'invalid color'
		return self._tiles[x // Tile.SIZE][y // Tile.SIZE].set_pixel_color(
			x % Tile.SIZE, y % Tile.SIZE, color
		)

	def set_pixel_num_locks(self, x, y, num_locks):
		assert 0 <= x < Tile.SIZE * Board.NUM_TILES, f'wrong x: {x}'
		assert 0 <= y < Tile.SIZE * Board.NUM_TILES, f'wrong y: {y}'
		assert 0 <= num_locks <= MAX_LOCKS, 'invalid num_locks'
		return self._tiles[x // Tile.SIZE][y // Tile.SIZE].set_pixel_num_locks(
			x % Tile.SIZE, y % Tile.SIZE, num_locks
		)

	def save(self):
		'''
		Save all changed tiles
		'''
		for x in range(self.NUM_TILES):
			for y in range(self.NUM_TILES):
				self._tiles[x][y].save()


@blueprint.route('/board/', methods=['GET'])
def board_get():
	known_hashes = request.headers.get('Tile-Hashes')
	if known_hashes is not None:
		if len(known_hashes) != HASH_LENGTH * Board.NUM_TILES ** 2:
			known_hashes = None
	return jsonify(Board(known_hashes=known_hashes).get_api_data())


BOARD_PATCH_SCHEMA = {
	'type': 'object',
	'anyOf': [
		{
			'properties': {
				'x': {
					'type': 'integer',
					'minimum': 0,
					'exclusiveMaximum': Tile.SIZE * Board.NUM_TILES
				},
				'y': {
					'type': 'integer',
					'minimum': 0,
					'exclusiveMaximum': Tile.SIZE * Board.NUM_TILES
				},
				'currColor': {
					'description': 'Current color of the pixel according to'
						' frontend',
					'type': 'integer',
					'minimum': 0,
					'exclusiveMaximum': NUM_COLORS
				},
				'action': {
					'enum': ['LOCK', 'UNLOCK']
				}
			},
			'required': ['x', 'y', 'currColor', 'action'],
			'additionalProperties': False
		},
		{
			'properties': {
				'x': {
					'type': 'integer',
					'minimum': 0,
					'exclusiveMaximum': Tile.SIZE * Board.NUM_TILES
				},
				'y': {
					'type': 'integer',
					'minimum': 0,
					'exclusiveMaximum': Tile.SIZE * Board.NUM_TILES
				},
				'currColor': {
					'description': 'Current color of the pixel according to'
						' frontend',
					'type': 'integer',
					'minimum': 0,
					'exclusiveMaximum': NUM_COLORS
				},
				'action': {
					'enum': ['CHANGE']
				},
				'nextColor': {
					'type': 'integer',
					'minimum': 0,
					'exclusiveMaximum': NUM_COLORS
				}
			},
			'required': ['x', 'y', 'currColor', 'action', 'nextColor'],
			'additionalProperties': False
		}
	]
}

@blueprint.route('/board/', methods=['PATCH'])
@state.transaction
def board_patch(pipe):
	request_json = request.json
	try:
		validate(instance=request_json, schema=BOARD_PATCH_SCHEMA)
	except ValidationError as e:
		return jsonify(e.message), 400

	x = request_json['x']
	y = request_json['y']
	currColor = request_json['currColor']
	action = request_json['action']
	nextColor = request_json.get('nextColor')

	pipe.watch(f'{BOARD_KEY_BASE}.{x // Tile.SIZE}.{y // Tile.SIZE}')

	known_hashes = request.headers.get('Tile-Hashes')
	if known_hashes is not None:
		if len(known_hashes) != HASH_LENGTH * Board.NUM_TILES ** 2:
			known_hashes = None
	board = Board(state_manager=pipe, known_hashes=known_hashes)

	success = True
	if currColor != board.get_pixel_color(x, y):
		success = False
	elif action == 'LOCK':
		num_locks = board.get_pixel_num_locks(x, y)
		if num_locks == MAX_LOCKS:
			success = False
		else:
			board.set_pixel_num_locks(x, y, num_locks + 1)
	elif action == 'UNLOCK':
		num_locks = board.get_pixel_num_locks(x, y)
		if num_locks == 0:
			success = False
		else:
			board.set_pixel_num_locks(x, y, num_locks - 1)
	else:  # if action == 'CHANGE':
		num_locks = board.get_pixel_num_locks(x, y)
		if num_locks != 0:
			success = False
		else:
			board.set_pixel_color(x, y, nextColor)

	if success:
		pipe.multi()
		board.save()

	return jsonify({
		'success': success,
		'tiles': board.get_api_data()
	})


@blueprint.route('/chat/', methods=['GET'])
def chat_get():
	chat_full = json.loads(state.get(CHAT_KEY))
	chat_tail = chat_full[-NUM_MESSAGES_TO_SEND:]
	return jsonify(chat_tail)


CHAT_POST_SCHEMA = {
	'type': 'object',
	'properties': {
		'n': {
			'description': 'Nick',
			'type': 'string',
			'maxLength': 32
		},
		'm': {
			'description': 'Message',
			'type': 'string',
			'maxLength': 256
		}
	},
	'required': ['n', 'm'],
	'additionalProperties': False
}

@blueprint.route('/chat/', methods=['POST'])
@state.transaction
def chat_post(pipe):
	request_json = request.json
	try:
		validate(instance=request_json, schema=CHAT_POST_SCHEMA)
	except ValidationError as e:
		return jsonify(e.message), 400

	pipe.watch(CHAT_KEY)
	chat_full = json.loads(pipe.get(CHAT_KEY))
	chat_full.append(request_json)

	pipe.multi()
	pipe.set(CHAT_KEY, json.dumps(chat_full[-NUM_MESSAGES_TO_SAVE:]))

	chat_tail = chat_full[-NUM_MESSAGES_TO_SEND:]
	return jsonify(chat_tail)


# Set up missing Redis values

if not state.exists(f'{BOARD_KEY_BASE}.0.0'):
	b = Board(randomize_all=True)
	b.save()
if not state.exists(CHAT_KEY):
	state.set(CHAT_KEY, json.dumps([]))
