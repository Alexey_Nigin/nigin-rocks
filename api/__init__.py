# Basic Flask setup from: https://flask.palletsprojects.com/en/2.2.x/quickstart/
# Basic Redis setup from: https://redis.io/docs/clients/python/

from flask import Flask
from jsonschema import validate, ValidationError
import subprocess
from redis import Redis

from . import state
from . import pixellations, pixel

app = Flask(__name__)
app.register_blueprint(pixellations.blueprint)
app.register_blueprint(pixel.blueprint)


# Set custom values

if __name__ == '__main__':
	# TODO: This is probably broken rn, fix later

	# Allow pasting looooooong strings
	# https://stackoverflow.com/a/54409649
	subprocess.check_call(["stty","-icanon"])

	input_canvas = input('canvas: ')
	input_canvas = input_canvas.strip()
	if input_canvas == '':
		print('no canvas specified, skipping')
	elif '|' in input_canvas:
		print('assuming old format')
		pixels = input_canvas.split('|')
		pixels = [int(pixel) for pixel in pixels]
		assert len(pixels) == pixel.CANVAS_SIZE**2, 'wrong number of pixels'
		assert all(
			0 <= pixel < pixel.NUM_COLORS * (pixel.MAX_LOCKS + 1) for pixel in pixels
		), 'pixel out of bounds'
		canvas = [
			[
				{
					'color': pixels[y * pixel.CANVAS_SIZE + x] % pixel.NUM_COLORS,
					'num_locks': pixels[y * pixel.CANVAS_SIZE + x] // pixel.NUM_COLORS
				}
				for y in range(pixel.CANVAS_SIZE)
			]
			for x in range(pixel.CANVAS_SIZE)
		]
		state.set('canvas', pixel.zip_canvas(canvas))
		print('canvas saved!')
	else:
		print('assuming new format')
		# Check that the canvas decodes successfully, will raise if not
		pixel.unzip_canvas(input_canvas)
		state.set('canvas', input_canvas)
		print('canvas saved!')

	# Restore terminal settings to normality!
	subprocess.check_call(["stty","icanon"])
