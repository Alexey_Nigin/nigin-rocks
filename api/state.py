'''
State management utils
'''

from redis import Redis, WatchError
from functools import wraps

r = Redis(host='localhost', port=6379, decode_responses=True)


def exists(key):
	'''
	Return True iff key exists.
	'''
	return r.exists(key)

def get(key):
	'''
	Get the value for key.
	'''
	return r.get(key)


def set(key, value):
	'''
	Set key to value.
	'''
	r.set(key, value)


def transaction(func):
	'''
	Decorator to run func in a transaction.

	func is called with the Redis-py pipe as the first argument.

	Suggested format for func:

	def func(pipe):
		pipe.watch(key)
		value = pipe.get(key)

		modify(value)

		pipe.multi()
		pipe.set(key, value)
		# No need to call execute()
	'''
	# @wraps to prevent Flask from complaining:
	# "AssertionError: View function mapping is overwriting an existing endpoint
	# function: pixellations.wrapper"
	# See https://stackoverflow.com/a/42254713
	@wraps(func)
	def wrapper(*args, **kwargs):
		with r.pipeline() as pipe:
			# TODO: Time out eventually
			while True:
				try:
					return_value = func(pipe, *args, **kwargs)
					pipe.execute()
					return return_value
				except WatchError:
					continue

	return wrapper
