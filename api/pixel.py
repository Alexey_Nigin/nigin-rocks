from flask import Blueprint, request, jsonify
from jsonschema import validate, ValidationError
import string
from . import state


blueprint = Blueprint('pixel', __name__, url_prefix='/pixel')

BASE_64_ALPHABET = (
	string.ascii_uppercase + string.ascii_lowercase + string.digits + '-_'
)
CANVAS_SIZE = 64
NUM_COLORS = 8
MAX_LOCKS = 255


# PixelLock utils

def zip_canvas(canvas):
	canvas_zip_list = []

	for y in range(CANVAS_SIZE):
		for x in range(CANVAS_SIZE):
			color = canvas[x][y]['color']
			num_locks = canvas[x][y]['num_locks']
			single_number = NUM_COLORS * num_locks + color

			if single_number < 32:
				canvas_zip_list.append(BASE_64_ALPHABET[single_number])
			else:
				canvas_zip_list.append(
					BASE_64_ALPHABET[32 + single_number // 64]
					+ BASE_64_ALPHABET[single_number % 64]
				)

	return ''.join(canvas_zip_list)


def unzip_canvas(canvas_zip):
	canvas = [[None for y in range(CANVAS_SIZE)] for x in range(CANVAS_SIZE)]

	pointer = 0

	def read_char():
		nonlocal pointer
		assert pointer < len(canvas_zip), \
						f'canvas_zip ({canvas_zip}) is incomplete'
		char = canvas_zip[pointer]
		pointer += 1
		assert char in BASE_64_ALPHABET, f'char ({char}) is invalid'
		return BASE_64_ALPHABET.index(char)

	for y in range(CANVAS_SIZE):
		for x in range(CANVAS_SIZE):
			char_0 = read_char()
			if char_0 < 32:
				single_number = char_0
			else:
				char_1 = read_char()
				single_number = 64 * (char_0 - 32) + char_1
			color = single_number % NUM_COLORS
			num_locks = single_number // NUM_COLORS
			canvas[x][y] = {'color': color, 'num_locks': num_locks}

	assert pointer == len(canvas_zip), 'canvas_zip has extra data'
	return canvas


# PixelLock endpoints

@blueprint.route('/canvas/', methods=['GET'])
def pixel_lock_canvas_get():
	return jsonify(state.get('canvas'))


PIXEL_LOCK_CANVAS_PATCH_SCHEMA = {
	'type': 'object',
	'properties': {
		'x': {
			'type': 'integer',
			'minimum': 0,
			'exclusiveMaximum': CANVAS_SIZE
		},
		'y': {
			'type': 'integer',
			'minimum': 0,
			'exclusiveMaximum': CANVAS_SIZE
		},
		'color': {
			'type': 'integer',
			'minimum': 0,
			'exclusiveMaximum': NUM_COLORS
		}
	},
	'required': ['x', 'y', 'color'],
	'additionalProperties': False
}

@blueprint.route('/canvas/', methods=['PATCH'])
@state.transaction
def pixel_lock_canvas_patch(pipe):
	json = request.json
	try:
		validate(instance=json, schema=PIXEL_LOCK_CANVAS_PATCH_SCHEMA)
	except ValidationError as e:
		return jsonify(e.message), 400
	x = json['x']
	y = json['y']
	color = json['color']

	pipe.watch('canvas')
	canvas = unzip_canvas(pipe.get('canvas'))

	if color == canvas[x][y]['color']:
		canvas[x][y]['num_locks'] = min(
			canvas[x][y]['num_locks'] + 1, MAX_LOCKS
		)
	elif canvas[x][y]['num_locks'] == 0:
		canvas[x][y]['color'] = color
	else:
		canvas[x][y]['num_locks'] -= 1

	canvas_zip = zip_canvas(canvas)
	pipe.multi()
	pipe.set('canvas', canvas_zip)

	return jsonify(canvas_zip)


# Set up missing Redis values

if not state.exists('canvas'):
	blank_canvas = [
		[
			{'color': 0, 'num_locks': 0}
			for y in range(CANVAS_SIZE)
		]
		for x in range(CANVAS_SIZE)
	]
	state.set('canvas', zip_canvas(blank_canvas))
