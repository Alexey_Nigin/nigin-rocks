import unittest
from api import pixellations

import re


class FakeStateManager:
	def __init__(self):
		self.state = {}

	def exists(self, key):
		return key in self.state

	def get(self, key):
		return self.state[key]

	def set(self, key, value):
		self.state[key] = value


class TestTile(unittest.TestCase):
	def setUp(self):
		self._fake_state_manager = FakeStateManager()

	def test_contructor_load(self):
		'''
			Test Tile(x, y, randomize=False)
		'''
		self._fake_state_manager.state['pixellations.board.1.2'] = 'L' * 1024
		t = pixellations.Tile(1, 2, self._fake_state_manager)
		self.assertEqual(t._zip, 'L' * 1024)
		self.assertEqual(t._pixels[5][6]['color'], 3)
		self.assertEqual(t._pixels[5][6]['num_locks'], 1)

	def test_constructor_randomize(self):
		'''
			Test Tile(x, y, randomize=True)
		'''
		t = pixellations.Tile(1, 2, self._fake_state_manager, randomize=True)
		self.assertTrue(re.fullmatch('[A-H]{1024}', t._zip))

	def test_get_api_hash_zip(self):
		'''
		Test Tile.get_api_hash_zip()
		'''
		t = pixellations.Tile(
			1, 2, state_manager=self._fake_state_manager, randomize=True
		)
		self.assertEqual(t.get_api_hash_zip()[16:], t._zip)

		t.save()
		t1 = pixellations.Tile(
			1, 2, state_manager=self._fake_state_manager,
			known_hash=t._get_hash()
		)
		self.assertEqual(t1.get_api_hash_zip(), None)

	def test_get_set_pixel_color(self):
		'''
			Test Tile.(get|set)_pixel_color()
		'''
		t = pixellations.Tile(1, 2, self._fake_state_manager, randomize=True)
		t.set_pixel_color(5, 6, 3)
		self.assertEqual(t.get_pixel_color(5, 6), 3)
		t.set_pixel_color(5, 6, 4)
		self.assertEqual(t.get_pixel_color(5, 6), 4)

	def test_get_set_pixel_num_locks(self):
		'''
			Test Tile.(get|set)_pixel_num_locks()
		'''
		t = pixellations.Tile(1, 2, self._fake_state_manager, randomize=True)
		t.set_pixel_num_locks(5, 6, 0)
		self.assertEqual(t.get_pixel_num_locks(5, 6), 0)
		t.set_pixel_num_locks(5, 6, 255)
		self.assertEqual(t.get_pixel_num_locks(5, 6), 255)

	def test_save(self):
		'''
			Test Tile.save()
		'''
		t = pixellations.Tile(1, 2, self._fake_state_manager, randomize=True)
		t.save()
		self.assertEqual(
			self._fake_state_manager.state['pixellations.board.1.2'],
			t._zip
		)

		# No unsaved changes, shouldn't save now
		self._fake_state_manager.state['pixellations.board.1.2'] = 'not saved'
		t.save()
		self.assertEqual(
			self._fake_state_manager.state['pixellations.board.1.2'],
			'not saved'
		)

		# Changing pixel color creates unsaved changes
		t.set_pixel_color(5, 6, 3)
		t.save()
		self.assertEqual(
			self._fake_state_manager.state['pixellations.board.1.2'],
			t._zip
		)

		# Changing pixel num_locks creates unsaved changes
		self._fake_state_manager.state['pixellations.board.1.2'] = 'not saved'
		t.set_pixel_num_locks(5, 6, 0)
		t.save()
		self.assertEqual(
			self._fake_state_manager.state['pixellations.board.1.2'],
			t._zip
		)


class TestBoard(unittest.TestCase):
	def setUp(self):
		self._fake_state_manager = FakeStateManager()

	def test_contructor_load(self):
		'''
			Test Board(randomize_all=False)
		'''
		for x in range(8):
			for y in range(8):
				self._fake_state_manager.state[f'pixellations.board.{x}.{y}'] \
								= 'L' * 1024
		b = pixellations.Board(state_manager=self._fake_state_manager)
		self.assertEqual(b._tiles[3][4]._zip, 'L' * 1024)

	def test_constructor_randomize(self):
		'''
			Test Board(randomize_all=True)
		'''
		b = pixellations.Board(
			state_manager=self._fake_state_manager, randomize_all=True
		)
		for x in range(8):
			for y in range(8):
				self.assertTrue(
					re.fullmatch('[A-H]{1024}', b._tiles[x][y]._zip)
				)

	def test_get_api_data(self):
		'''
			Test Board.get_api_data()
		'''
		b = pixellations.Board(
			state_manager=self._fake_state_manager, randomize_all=True
		)
		api_data = b.get_api_data()
		self.assertEqual(len(api_data), 64)

		b.save()
		known_hashes = ''.join([tile_str[:16] for tile_str in api_data])
		b1 = pixellations.Board(
			state_manager=self._fake_state_manager, known_hashes=known_hashes
		)
		b1.save()
		self.assertEqual(b1.get_api_data(), [None for _ in range(64)])

	def test_get_set_pixel_color(self):
		'''
			Test Board.(get|set)_pixel_color()
		'''
		b = pixellations.Board(
			state_manager=self._fake_state_manager, randomize_all=True
		)
		b.set_pixel_color(55, 166, 3)
		self.assertEqual(b.get_pixel_color(55, 166), 3)
		b.set_pixel_color(55, 166, 4)
		self.assertEqual(b.get_pixel_color(55, 166), 4)

	def test_get_set_pixel_num_locks(self):
		'''
			Test Board.(get|set)_pixel_num_locks()
		'''
		b = pixellations.Board(
			state_manager=self._fake_state_manager, randomize_all=True
		)
		b.set_pixel_num_locks(55, 166, 0)
		self.assertEqual(b.get_pixel_num_locks(55, 166), 0)
		b.set_pixel_num_locks(55, 166, 255)
		self.assertEqual(b.get_pixel_num_locks(55, 166), 255)

	def test_save(self):
		'''
			Test Board.save()
		'''
		# Randomized board, save all tiles
		b = pixellations.Board(
			state_manager=self._fake_state_manager, randomize_all=True
		)
		b.save()
		for x in range(8):
			for y in range(8):
				self.assertEqual(
					self._fake_state_manager.state[
						f'pixellations.board.{x}.{y}'
					],
					b._tiles[x][y]._zip
				)

		# No unsaved changes, save no tiles
		for x in range(8):
			for y in range(8):
				self._fake_state_manager.state[f'pixellations.board.{x}.{y}'] \
								= 'not saved'
		b.save()
		for x in range(8):
			for y in range(8):
				self.assertEqual(
					self._fake_state_manager.state[
						f'pixellations.board.{x}.{y}'
					],
					'not saved'
				)

		# Change a pixel, save only one tile
		b.set_pixel_color(55, 166, 3)
		b.save()
		for x in range(8):
			for y in range(8):
				self.assertEqual(
					self._fake_state_manager.state[
						f'pixellations.board.{x}.{y}'
					],
					b._tiles[x][y]._zip if (x == 1 and y == 5) else 'not saved'
				)


if __name__ == '__main__':
	unittest.main()
