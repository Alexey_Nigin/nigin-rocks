FROM alpine:3.18 AS static-builder
# RUN apk add --no-cache bash
RUN apk add --update git npm
RUN npm install -g less minify uglify-js
COPY static-builder /source
RUN sh /source/build-static.sh
COPY nginx.conf.in .url /
RUN url=$(cat /.url) && sed "s REPLACE_URL $url " /nginx.conf.in > /nginx.conf



FROM nginx AS final

## redis install (https://redis.io/docs/getting-started/installation/install-redis-on-linux/)
RUN apt update
RUN apt install -y lsb-release
##RUN curl -fsSL https://packages.redis.io/gpg | gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg
##RUN echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list
RUN apt update
RUN apt -y install redis

# snap installs don't work in docker :(
RUN apt install -y gunicorn
RUN apt install -y python3-pip
RUN pip install flask redis[hiredis] jsonschema certbot certbot-nginx --break-system-packages

COPY --from=static-builder /static /usr/share/nginx/html
COPY api /api
COPY wsgi.py /
COPY --from=static-builder /nginx.conf /etc/nginx/nginx.conf
COPY redis.conf start.sh /
ENV PYTHONPATH=/
ENV RUN_CERTBOT= CERTBOT_EMAIL=
CMD ["sh", "/start.sh"]
