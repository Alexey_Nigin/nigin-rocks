# Start the server
# Execute with /bin/sh, only in docker!

set -e

redis-server /redis.conf &
gunicorn --log-file /gunicorn-error.log --workers=2 wsgi:app &
nginx -g 'daemon off;' &
if [ -n "$RUN_CERTBOT" ]; then
	certbot -n --nginx --domains nigin.rocks,www.nigin.rocks --keep \
	--redirect --hsts \
	--agree-tos -m "$CERTBOT_EMAIL" --no-eff-email &
fi

while true; do sleep 1; done
