# From: https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04#creating-the-wsgi-entry-point
from api import app

if __name__ == "__main__":
	app.run()
