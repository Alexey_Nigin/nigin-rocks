function isEven(n) {
	return n % 2 === 0;
}

// Return [tugLength, tugsX, tugsY]
function getTugInfo(pixelsX, pixelsY, numDiamonds) {
	let tugsX = 2;
	let tugsY = 2;
	let tugLength;
	while (true) {
		tugLength = Math.min(pixelsX / tugsX, pixelsY / tugsY);
		let trianglesTotal = 2 * tugsX * tugsY;
		let trianglesBorder = 2 * (tugsX + tugsY)
						- ((isEven(tugsX) && isEven(tugsY)) ? 4 : 2);
		let trianglesFree = trianglesTotal - trianglesBorder;
		console.assert(trianglesFree % 4 === 0);
		let diamondsFree = trianglesFree / 4;
		if (diamondsFree >= numDiamonds) break;
		if (pixelsX / (tugsX + 1) > pixelsY / (tugsY + 1)) {
			++tugsX;
		} else {
			++tugsY;
		}
	}

	return [tugLength, tugsX, tugsY];
}

function arrangeTiles(startX, startY, width, height, margin) {
	let [tugLength, tugsX, tugsY] = getTugInfo(width + margin, height + margin, 19);

	let elTiles = document.getElementsByClassName('tile');

	let x = 0;
	let y = 0;
	let size = `${2 * tugLength - margin}px`;
	for (let elTile of elTiles) {
		elTile.style.width = size;
		elTile.style.height = size;
		elTile.style.lineHeight = size;
		elTile.style.left = (startX + x * tugLength) + 'px';
		elTile.style.top = (startY + y * tugLength) + 'px';
		elTile.style.fontSize = `${0.2 * tugLength}px`;
		x += 2;
		if (x > (tugsX - 2)) {
			++y;
			x = y % 2;
		}
	}
}

function arrangePage() {
	let width = window.innerWidth;
	let height = window.innerHeight;
	let minDimension = Math.min(width, height);
	let margin = Math.round(0.02 * minDimension);

	let headerHeight = Math.round(0.1 * minDimension);
	let elHeader = document.getElementsByTagName('header')[0];
	// TODO: Use cssText to clean this up
	elHeader.style.margin = `${margin}px`;
	elHeader.style.height = headerHeight + 'px';
	elHeader.style.lineHeight = headerHeight + 'px';
	elHeader.style.fontSize = `${headerHeight / 2}px`;

	let anchorLeft = headerHeight / 2;
	let anchorRight = elHeader.clientWidth - anchorLeft;
	elHeader.style.clipPath = `polygon(${anchorLeft}px 0%, ${anchorRight}px 0%, 100% 50%, ${anchorRight}px 100%, ${anchorLeft}px 100%, 0% 50%)`;

	arrangeTiles(
		margin,
		headerHeight + 2 * margin,
		width - 2 * margin,
		height - headerHeight - 3 * margin,
		Math.sqrt(2) * margin
	);
}

window.addEventListener('resize', arrangePage);
arrangePage();
