# Build /static from /source
# Execute with /bin/sh, only in docker!

set -e

mkdir /static

# Pixellations

mkdir /static/pixellations
cd /source/pixellations
cp -rf *.html style images scripts /static/pixellations

# pixel

mkdir /static/pixel
cd /source/pixel
cp *.html *.css *.png *.js /static/pixel

# guesstimator

git clone https://gitlab.com/Alexey_Nigin/guesstimator.git /source/guesstimator
cd /source/guesstimator
mkdir /static/guesstimator

# v0.1.0
mkdir /static/guesstimator/v0.1.0
git checkout v0.1.0
cp -rf *.html *.css images* scripts /static/guesstimator/v0.1.0

# v0.2.0
mkdir /static/guesstimator/v0.2.0
git checkout v0.2.0
cp -rf *.html *.css images* scripts /static/guesstimator/v0.2.0

# v0.3.0
mkdir /static/guesstimator/v0.3.0
git checkout v0.3.0
cp -rf *.html *.css images* scripts /static/guesstimator/v0.3.0

# v0.4.0
mkdir /static/guesstimator/v0.4.0
git checkout v0.4.0
cp -rf *.html *.css images* scripts /static/guesstimator/v0.4.0

# v0.5.0
mkdir /static/guesstimator/v0.5.0
git checkout v0.5.0
cp -rf *.html *.css images* scripts /static/guesstimator/v0.5.0

# v0.6.0
mkdir /static/guesstimator/v0.6.0
git checkout v0.6.0
cp -rf *.html *.css images scripts /static/guesstimator/v0.6.0

# v0.7.0
mkdir /static/guesstimator/v0.7.0
git checkout v0.7.0
cp -rf *.html *.css images scripts /static/guesstimator/v0.7.0

# v0.8.0
mkdir /static/guesstimator/v0.8.0
git checkout v0.8.0
cp -rf *.html *.css images scripts /static/guesstimator/v0.8.0

# v0.9.0
mkdir /static/guesstimator/v0.9.0
git checkout v0.9.0
cp -rf *.html *.css images scripts /static/guesstimator/v0.9.0

# v0.10.0
mkdir /static/guesstimator/v0.10.0
git checkout v0.10.0
mkdir dist
npm install
npm run build
cp -rf dist/* /static/guesstimator/v0.10.0

# v0.11.0
mkdir /static/guesstimator/v0.11.0
git clean -fdx
git checkout v0.11.0
mkdir dist
npm install
npm run build
cp -rf dist/* /static/guesstimator/v0.11.0

# v0.12.0
mkdir /static/guesstimator/v0.12.0
git clean -fdx
git restore .
git checkout v0.12.0
mkdir dist
npm install
npm run build
cp -rf dist/* /static/guesstimator/v0.12.0

# v0.13.0
mkdir /static/guesstimator/v0.13.0
git clean -fdx
git restore .
git checkout v0.13.0
mkdir dist
npm install
npm run build
cp -rf dist/* /static/guesstimator/v0.13.0


# latest
cp -rf /static/guesstimator/v0.13.0/* /static/guesstimator

# O

mkdir /static/o
cd /source/o
cp -rf index.html background.jpg /static/o

# standoff

mkdir /static/standoff
cd /source/standoff
cp *.html *.css *.jpg /static/standoff

# Timeline

mkdir /static/timeline
cd /source/timeline
cp -rf *.html *.css images *.js /static/timeline

# main

cd /source/main
minify index.html > /static/index.html
lessc style.less style.css
minify style.css > /static/style.css
mkdir /static/scripts
# TODO: Serve the source map correctly
uglifyjs scripts/main-menu.js --compress --mangle --toplevel --source-map --output /static/scripts/main-menu.js
