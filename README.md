# nigin.rocks

The current version of this README is designed to only work for / make sense to
me, Alexey Nigin.

## Spinning up a Droplet

### Create the Droplet

1. Choose region - New York.
2. Choose datacenter - random number from 1 to 3.
3. Choose image - Ubuntu 22.10 x64.
4. Choose size - Shared CPU, Regular, $4/month plan.
5. Choose authentication method - SSH.
6. Choose extras - yes to monitoring, yes to backups, no to database.
   * TODO: Implement Redis-only backups.
7. Choose name - `nigin-rocks-prod`.

### Set up Ubuntu

```
local$   ssh root@<Droplet IP>
droplet# adduser alexey
droplet# usermod -aG sudo alexey
droplet# ufw allow OpenSSH
droplet# ufw enable
droplet# ufw status  # OpenSSH ALLOW from Anywhere
# Won't need to change firewall settings after this, since Docker bypasses it
droplet# rsync --archive --chown=alexey:alexey ~/.ssh /home/alexey
local$   ssh alexey@<Droplet IP>  # Don't close the root shell yet!
droplet$ sudo echo hallo  # Okay if this succeeds you can close the root shell
```

_Abridged from a
[DigitalOcean tutorial](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-22-04)._

### Set up Docker

```shell
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
apt-cache policy docker-ce  # Should have "download.docker.com"
sudo apt install docker-ce
sudo systemctl status docker  # Should be "active (running)"
sudo usermod -aG docker ${USER}
su - ${USER}
groups  # Should have "docker" for my user
```

_Abridged from a
[DigitalOcean tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04)._

### Run the server

`ssh -A` to the Droplet or else cloning via SSH will be sad.

```shell
cd
git clone --recurse-submodules git@gitlab.com:Alexey_Nigin/nigin-rocks.git
cd nigin-rocks
./deploy/build
./deploy/productize
```

### Update data

```
droplet$ docker container ls  # Find nrc_num
droplet$ docker exec -it nrc-<nrc_num> bash
nrc$     python3 app.py  # enter new data
```

## Installing Docker locally

I am running `Ubuntu 20.04`.

```shell
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
apt-cache policy docker-ce  # Should have "download.docker.com"
sudo apt install docker-ce
sudo systemctl status docker  # Should be "active (running)"
sudo usermod -aG docker ${USER}
su - ${USER}
groups  # Should have "docker" for my user
```

_Abridged from a
[DigitalOcean tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04)._

## License

All of this project (aside from small code snippets credited inline) was created
by Alexey Nigin and is available under the
[Creative Commons Attribution-NonCommercial](https://creativecommons.org/licenses/by-nc/4.0/)
license.
